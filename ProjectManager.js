/*
* Copyright 2012 Ryan Ackley (ryanackley@gmail.com)
*
* This file is part of Tincr.
*
* Tincr is free software: you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
ProjectManager = function(){
	this.projectsByTab = {};
	this.watchersByTab = {};
}

ProjectManager.prototype = {
    _initProject : function(tabId, path, url, projectType, urlDataMap, sendResponse){
    	var self = this;
    	var projectsByTab = this.projectsByTab;
    	this.cleanUp(tabId);
    	window.requestFileSystem(window.PERMANENT, 5*1024*1024*1024, function(fs){
			fs.root.getDirectory(path, {create:false}, function(dir){
				projectType.createProject(dir,url, function(project, error){
					self.fsRoot = fs.root;
					if (project){
						// // we map resources to files only once on initial loading of the tincr panel then cache it in this data structure
						// project.matchedResourceMap = {};

						// // This is for custom mapped resources
						// project.customResourceMap = urlPathMap || {};

						// this tells the project type to store the custom mappings for auto reloading purposes
						for (var url in urlDataMap){
							project.mapDataToUrl(url, urlDataMap[url]);
						}

						if (!project.compare){
							project.compare = function(a,b){
								return a === b;
							}
						}
						projectsByTab[tabId] = project;
					}
					sendResponse({path:path, error:error});
				});
			});
		});
    },
    
	launchFolderSelect : function(tabId, url, typeIndex, sendResponse){
		var self = this;
		nativeFileSupport.launchFolderSelect(function(path){
			if (path && path.length){
				var projectType = ProjectTypes[typeIndex];
				self._initProject(tabId, path, url, projectType, {}, sendResponse);
			}
		});
	},

	launchFileSelect : function(tabId, url, op, sendResponse){
		var currentProject = this.projectsByTab[tabId];
		var self = this;
		if (currentProject){
			nativeFileSupport.launchFileSelect(function(path){
				if (path && path.length){
					if (op == 'autosave'){
						var data = {autoSave: path};
						self._setPathData(currentProject, url, data, sendResponse);
					}
					else if (op == 'add-autorefresh'){
						var data = currentProject.filePathsForUrl(url);
						if (data.autoSave){
							delete data.autoSave;
						}

						data.autoReload = data.autoReload || [];
						for (var i = 0; i < data.autoReload.length; i++){
							if (data.autoReload[i] == path){
								sendResponse(data);
								return;
							}
						}
						data.autoReload.push(path);
						self._setPathData(currentProject, url, data, sendResponse);
					}
				}
			});
		}
	},
	clearResource : function(tabId, url, sendResponse){
		this.setCustomPathData(tabId, url, {}, sendResponse);
	},
	setCustomPathData : function(tabId, url, data, sendResponse){
		var currentProject = this.projectsByTab[tabId];
		this._setPathData(currentProject, url, data, sendResponse);
	},
	_setPathData : function(currentProject, url, data, sendResponse){
		currentProject.clearUrlCache(url);
		currentProject.mapDataToUrl(url, data);
		sendResponse(data);
	},
	setResourceOptions : function(tabId, url, exactMatch, sendResponse){
		var currentProject = this.projectsByTab[tabId];
		var data = currentProject.filePathsForUrl(url);
		if (exactMatch){
			if (data.autoReload && data.autoReload.length >= 1){
				data.autoSave = data.autoReload[0];
				var dependencies = data.autoReload.slice(1);
				if (dependencies && dependencies.length){
					data.old = data.old || {};
					data.old.dependencies = dependencies;
				}
				delete data.autoReload;
			}
		}
		else {
			if (data.autoSave){
				data.autoReload = data.autoReload || [];
				data.autoReload[0] = data.autoSave;
				if (data.old && data.old.dependencies){
					data.autoReload = data.autoReload.concat(data.old.dependencies);
				}
				delete data.autoSave;
			}
		}
		this._setPathData(currentProject, url, data, sendResponse);
	},
	removeDependency : function(tabId, url, index, sendResponse){
		var currentProject = this.projectsByTab[tabId];
		var data = currentProject.filePathsForUrl(url);
		data.autoReload = data.autoReload || [];
		if (data.autoReload[index]){
			data.autoReload.splice(index, 1);
		}
		currentProject.mapDataToUrl(url, data);	
		sendResponse(data);		
	},
	checkResources : function(tabId, urls, sendResponse){
		var currentProject = this.projectsByTab[tabId];
		var retVal = [];
		if (currentProject){
			for (var i = 0; i < urls.length; i++){
				if (urls[i].type == 'document' || urls[i].type == 'script' || urls[i].type == 'stylesheet'){
					var url = urls[i].url;
				    var obj = currentProject.filePathsForUrl(url);
				    if (obj && (obj.autoSave || obj.autoReload)){
				    	retVal.push(obj);
				    }
				    else{
				    	retVal.push(null);
				    }
				}
				else{
					retVal.push(null);
				}
			}
		}
		sendResponse(retVal);
	},
	
	checkResourceContent : function(tabId, url, content, sendResponse){
		var currentProject = this.projectsByTab[tabId];
		if (currentProject){
			var obj = currentProject.filePathsForUrl(url);//currentProject.matchedResourceMap[url];
			var path = obj.autoSave;
			if (path){
				Gito.FileUtils.readFile(this.fsRoot, path, 'Text', function(localContent){
					if (!currentProject.compare(content, localContent)){
					
						var ran = Math.round(Math.random()*10000000);
						var handleError = function(){
							sendResponse({success: false, msg: 'Content for url ' + url + ' doesn\'t match local file ' + path});
							
							//TODO handle this on the front end with a warning.
							//delete currentProject.matchedResourceMap[url];
						}
						
						var handleContent = function(newContent){
							if (!currentProject.compare(newContent,localContent)){
								handleError();
							}else{
								sendResponse({success: true, msg: 'Content for url ' + url + ' was reloaded and it matches local file ' + path, content: newContent });
							}
						}
						
						$.ajax({type: 'GET',
							url: url + (url.indexOf('?') != -1 ? '&' : '?') + 'r=' + ran,
							dataType: 'text',
							success: handleContent,
							error: handleError}); 
						
					}
					else{
						sendResponse({success: true, msg: 'Content for url ' + url + ' matches local file ' + path });
					}
				}, function(){
					sendResponse({success: false, msg: 'Can\'t open and read file at path ' + path})
				});
			}
			else{
				sendResponse({success: false, msg: 'Can\'t find path for url ' + url});
			}
		}
		else{
			sendResponse({success: false, msg: 'Can\'t find a current project for the tab'});
		}
	},
	
	updateResource: function(tabId, url, content, sendResponse){
		var currentProject = this.projectsByTab[tabId];
		var watcher = this.watchersByTab[tabId];
		//currentProject.commitResource(url, content, currentWatcher, sendResponse);
		var obj = currentProject.filePathsForUrl(url);
		var localPath = obj.autoSave;
		if (localPath){
			var dotIdx = localPath.lastIndexOf('.');
			if (dotIdx != -1){
				var ext = localPath.substring(dotIdx + 1);
				if (watcher && (ext == 'js' || ext == 'css' || ext == 'html' || ext == 'htm')){
					watcher.addChangedByMe(localPath);
				}
			}
			Gito.FileUtils.mkfile(this.fsRoot, localPath, content, sendResponse);
		}
		else{
			sendResponse();
		}
	},
	loadProject : function(tabId, projectTypeKey, path, url, urlPathMap,sendResponse){
		if (projectTypeKey == 'fileUrl'){
			this._initProject(tabId, path, url, FileUrlProjectFactory, {}, sendResponse);
		}
		else{		
			for (var i = 0; i < ProjectTypes.length; i++){
				if (ProjectTypes[i].key === projectTypeKey){
					this._initProject(tabId, path, url, ProjectTypes[i], urlPathMap, sendResponse);
					break;
				}
			}
		}
	},
	resetProject : function(tabId, sendResponse){
		var currentProject = this.projectsByTab[tabId];
		currentProject.resetUrls();
		sendResponse();
	},
	watchDirectory : function(port){
		var path = port.name;
		var self = this;
		port.onMessage.addListener(function(msg){
			var tabId = msg.tabId;
			if (!self.watchersByTab[tabId]){
				var currentProject = self.projectsByTab[tabId];
				self.watchersByTab[tabId] = new FileWatcher(port, tabId, currentProject, path, self.fsRoot);
				//sendResponse();
			}
		});
		
	},
	
	unwatchDirectory : function(tabId, sendResponse){
		this._unwatch(tabId);
		sendResponse();
	},
	_unwatch : function(tabId){
		var watcher = this.watchersByTab[tabId];
		if (watcher){
			watcher.stopWatching();
			delete this.watchersByTab[tabId];
		}
	},
	cleanUp : function(tabId){
		if (this.projectsByTab[tabId])
			delete this.projectsByTab[tabId];
		  			
		this._unwatch(tabId);
	}
}